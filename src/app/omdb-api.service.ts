import { OmdbApiResponse } from "./omdb-api-response";
import { Movie } from "./movie";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class OmdbApiService {
  readonly apiUrl = 'https://www.omdbapi.com/';
  readonly apiKey = '2c9d65d5';
  
  constructor(private httpClient: HttpClient) {}

  async searchMovies(title: string, page: number = 1): Promise<OmdbApiResponse> {
    let apiParams = {
      apiKey: this.apiKey,
      s: title,
      page: page.toString()
    };

    return this.httpClient.get<OmdbApiResponse>(this.apiUrl, { params: apiParams }).toPromise();
  }

  async getMovieDetails(id: string): Promise<Movie> {
    let apiParams = {
      apiKey: this.apiKey,
      i: id,
      plot: 'full'
    };

    return this.httpClient.get<Movie>(this.apiUrl, { params: apiParams }).toPromise();
  }
}
