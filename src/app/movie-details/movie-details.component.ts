import { Component, OnInit } from '@angular/core';
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { OmdbApiService } from '../omdb-api.service';
import { Movie } from '../movie';
import { ActivatedRoute } from '@angular/router';
import { FavoritesStorageService } from '../favorites-storage.service';

@Component({
  selector: 'ns-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css'],
  moduleId: module.id,
})
export class MovieDetailsComponent implements OnInit {
  movie: Movie;

  constructor(
    private route: ActivatedRoute,
    private omdbApi: OmdbApiService,
    private favoritesStorage: FavoritesStorageService) { }

  async ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.movie = await this.omdbApi.getMovieDetails(id);
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  onFavorite() {
    this.favoritesStorage.addMovie(this.movie);
  }
}
