import { Component, OnInit } from '@angular/core';
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { OmdbApiService } from '../omdb-api.service';
import { Movie } from '../movie';

@Component({
  selector: 'ns-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.css'],  
  moduleId: module.id,
})
export class MovieSearchComponent implements OnInit {
  movieName: string = "Matrix";
  result: string;
  movies: Array<Movie>;

  constructor(private omdbApi: OmdbApiService) { }

  ngOnInit() {
  }

  async onSearch() {
    let response = await this.omdbApi.searchMovies(this.movieName);
    this.result = response.totalResults;
    this.movies = response.Search;
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

}
