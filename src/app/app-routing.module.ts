import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { FavoritesComponent } from "./favorites/favorites.component";
import { MovieSearchComponent } from "./movie-search/movie-search.component";
import { MovieDetailsComponent } from "./movie-details/movie-details.component";

const routes: Routes = [
    { path: "", redirectTo: "/search", pathMatch: "full" },
    { path: "favorites", component: FavoritesComponent },
    { path: "search", component: MovieSearchComponent },
    { path: "details/:id", component: MovieDetailsComponent }
    
    // { path: "home", loadChildren: "~/app/home/home.module#HomeModule" },
    // { path: "browse", loadChildren: "~/app/browse/browse.module#BrowseModule" },
    // { path: "search", loadChildren: "~/app/search/search.module#SearchModule" },
    // { path: "featured", loadChildren: "~/app/featured/featured.module#FeaturedModule" },
    // { path: "settings", loadChildren: "~/app/settings/settings.module#SettingsModule" },

];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
