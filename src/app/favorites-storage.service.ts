import { Injectable } from '@angular/core';
import { Movie } from './movie';
import * as appSettings from "tns-core-modules/application-settings";

@Injectable({
  providedIn: 'root'
})
export class FavoritesStorageService { 
  
  addMovie(movie: Movie): void {
    let movies: Array<Movie>; 
    let moviesJson = appSettings.getString('favorites');
    if (moviesJson) {
      movies = JSON.parse(moviesJson);
    } else {
      movies = new Array<Movie>();
    }
    movies.push(movie);
    appSettings.setString('favorites', JSON.stringify(movies));
    // localStorage.setItem('favorites', JSON.stringify(movies));
  }

  removeMovie(movie: Movie): void {
    let movies: Array<Movie>;
    let moviesJson = appSettings.getString('favorites');
    // let moviesJson = localStorage.getItem('favorites');
    if (moviesJson) {
      movies = JSON.parse(moviesJson);
      movies = movies.filter(x => x.imdbID != movie.imdbID);
      appSettings.setString('favorites', JSON.stringify(movies));
      // localStorage.setItem('favorites', JSON.stringify(movies));
    }
  }

  getAllFavorites(): Array<Movie> {
    let movies: Array<Movie>;
    let moviesJson = appSettings.getString('favorites');
    // let moviesJson = localStorage.getItem('favorites');
    if (moviesJson) {
      movies = JSON.parse(moviesJson);
    } else {
      movies = new Array<Movie>();
    }
    return movies;
  }
}
