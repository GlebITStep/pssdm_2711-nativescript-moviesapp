import { Component, OnInit } from '@angular/core';
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { FavoritesStorageService } from '../favorites-storage.service';
import { Movie } from '../movie';


@Component({
  selector: 'ns-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css'],
  moduleId: module.id,
})
export class FavoritesComponent implements OnInit {
  movies: Array<Movie>;

  constructor(
    private favoritesStorage: FavoritesStorageService
  ) { }

  ngOnInit() {
    this.movies = this.favoritesStorage.getAllFavorites();
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }


}
